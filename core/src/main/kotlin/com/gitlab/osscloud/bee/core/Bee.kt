package com.gitlab.osscloud.bee.core

import com.gitlab.osscloud.bee.core.execute.Executor
import com.gitlab.osscloud.bee.core.plan.Planner
import com.gitlab.osscloud.bee.core.workspace.WorkspaceFactory
import java.nio.file.Files

class Bee(
    private val options: BeeOptions,
) {
    companion object {
        const val ROOT_FILENAME = "bee.queen.yml"
        const val BUILD_FILENAME = "bee.yml"
    }

    init {
        require(options.concurrent >= 1) { "concurrent must be at least 1" }
        require(Files.exists(options.rootPath.resolve(ROOT_FILENAME))) { "invalid root path, or $ROOT_FILENAME not exists in root path" }
    }

    @JvmName("buildForLiteralTargets")
    fun buildForTargets(targets: List<String>) {
        buildForTargets(targets.map { ExecuteTarget.fromFullName(it) })
    }

    fun buildForTargets(targets: List<ExecuteTarget>) {
        WorkspaceFactory.build(options)
            .let { Planner.plan(it, targets) }
            .let { Executor.execute(it) }
    }
}
