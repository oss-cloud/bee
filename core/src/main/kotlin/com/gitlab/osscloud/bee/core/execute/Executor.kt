package com.gitlab.osscloud.bee.core.execute

import com.github.dexecutor.core.ExecutionConfig
import com.gitlab.osscloud.bee.core.plan.Plan

internal object Executor {
    @JvmStatic
    fun execute(plan: Plan) {
        plan.executor.execute(ExecutionConfig.TERMINATING)
    }
}
