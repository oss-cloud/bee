package com.gitlab.osscloud.bee.core.workspace

import com.gitlab.osscloud.bee.core.plugin.PluginMethodProvider
import java.nio.file.Path

internal data class Workspace(
    val rootPath: Path,
    val concurrent: Int,
    val pluginMethods: Map<String, PluginMethodProvider>,
)
