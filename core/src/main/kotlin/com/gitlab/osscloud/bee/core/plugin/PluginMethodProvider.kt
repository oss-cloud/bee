package com.gitlab.osscloud.bee.core.plugin

import com.gitlab.osscloud.bee.spi.plugin.Plugin
import com.gitlab.osscloud.bee.spi.plugin.PluginMethod

internal class PluginMethodProvider(
    private val name: String,
    private val plugin: Plugin,
    private val options: Map<String, String>,
) {
    fun create(): PluginMethod {
        return plugin.createMethod(name, options)
    }

    fun create(overrideOptions: Map<String, String>): PluginMethod {
        return plugin.createMethod(name, options + overrideOptions)
    }
}
