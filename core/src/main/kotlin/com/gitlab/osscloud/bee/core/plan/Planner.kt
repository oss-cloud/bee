package com.gitlab.osscloud.bee.core.plan

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.github.dexecutor.core.DefaultDexecutor
import com.github.dexecutor.core.DefaultDexecutorState
import com.github.dexecutor.core.DexecutorConfig
import com.github.dexecutor.core.ExecutionListener
import com.github.dexecutor.core.task.Task
import com.gitlab.osscloud.bee.core.Bee
import com.gitlab.osscloud.bee.core.ExecuteTarget
import com.gitlab.osscloud.bee.core.execute.BeeExecutionEngine
import com.gitlab.osscloud.bee.core.execute.BeeTaskProvider
import com.gitlab.osscloud.bee.core.execute.ExecutionTask
import com.gitlab.osscloud.bee.core.execute.TaskOutput
import com.gitlab.osscloud.bee.core.workspace.Workspace
import com.gitlab.osscloud.bee.spi.plugin.BeeExecuteContext
import com.gitlab.osscloud.bee.spi.plugin.PluginMethod
import org.slf4j.LoggerFactory
import java.lang.Exception
import java.util.Queue
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.Executors

internal object Planner {
    private val log = LoggerFactory.getLogger(this::class.java)
    private val objectMapper = ObjectMapper(YAMLFactory()).findAndRegisterModules().registerKotlinModule()

    @JvmStatic
    fun plan(ws: Workspace, targets: List<ExecuteTarget>): Plan {
        val resolvedTarget = mutableSetOf<ExecuteTarget>()
        val unresolvedDependencies = hashMapOf<ExecuteTarget, List<ExecuteTarget>>()
        val pendingTarget: Queue<ExecuteTarget> = ConcurrentLinkedQueue(targets)

        val taskProvider = BeeTaskProvider()
        val engine = BeeExecutionEngine(
            DefaultDexecutorState<String, TaskOutput?>(),
            Executors.newFixedThreadPool(ws.concurrent),
            BeeExecutionListener,
        )
        val methods = hashMapOf<String, PluginMethod>()

        val config = DexecutorConfig<String, TaskOutput?>(engine, taskProvider)

        val executor = DefaultDexecutor(config)

        while (pendingTarget.isNotEmpty()) {
            val target = pendingTarget.poll()
            if (resolvedTarget.contains(target)) {
                continue
            }

            if (unresolvedDependencies.containsKey(target)) {
                val dependencies = unresolvedDependencies[target]!!
                pendingTarget.addAll(dependencies)
                resolvedTarget.add(target)
                log.info("{} adding dependencies: {}", target, dependencies)
                continue
            }

            log.info("start resolving target: {}", target)

            val expectedPath = ws.rootPath.resolve(target.path.substring(2))
            val expectedFile = expectedPath.resolve(Bee.BUILD_FILENAME).toFile()

            log.info("parsing {} for targets", expectedFile)

            val definition = objectMapper.readValue<BuildDefinition>(expectedFile)
            definition.targets.forEach {
                val isUnknownMethod = it.method != null && !ws.pluginMethods.containsKey(it.method)
                if (it.name == target.label && isUnknownMethod) {
                    throw RuntimeException("unknown method ${it.method}")
                }

                if (!isUnknownMethod) {
                    val task = ExecutionTask(
                        path = target.path,
                        method = if (it.method == null) {
                            VirtualPluginMethod
                        } else if (methods.containsKey(it.method)) {
                            methods[it.method]!!
                        } else {
                            val instance = ws.pluginMethods[it.method]!!.create()
                            methods[it.method] = instance
                            instance
                        },
                        cwd = expectedPath,
                        args = it.args ?: emptyMap(),
                        output = it.output.toSet(),
                    )
                    taskProvider.registerTask(ExecuteTarget(target.path, it.name), task)
                    log.info("register task: {} in path {}", it.name, target.path)
                }
                val dependencies = it.dependencies.map { dep ->
                    ExecuteTarget.resolveWithContext(target.path, dep)
                }

                if (it.name == target.label) {
                    log.info("{} adding dependencies: {}", target, dependencies)
                    pendingTarget.addAll(dependencies)
                    dependencies.forEach { dep ->
                        executor.addDependency(dep.toString(), target.toString())
                    }
                    resolvedTarget.add(target)
                } else {
                    unresolvedDependencies[ExecuteTarget(target.path, it.name)] = dependencies
                }
            }
        }
        return Plan(executor)
    }

    private object BeeExecutionListener : ExecutionListener<String, TaskOutput?> {
        private val log = LoggerFactory.getLogger(BeeExecutionListener::class.java)

        override fun onError(task: Task<String, TaskOutput?>, exception: Exception) {
            log.error("error occur during execute target {}, cause: {}", task.id, exception)
        }

        override fun onSuccess(task: Task<String, TaskOutput?>) {
            log.info("successful execute {}", task.id)
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private data class BuildDefinition(
        val targets: List<BuildTargetSpec>,
    ) {
        @JsonIgnoreProperties(ignoreUnknown = true)
        data class BuildTargetSpec(
            val name: String,
            val method: String?,
            val dependencies: List<String> = emptyList(),
            val args: Map<String, String>?,
            val output: List<String> = emptyList(),
        )
    }

    private object VirtualPluginMethod : PluginMethod {
        override val name: String = "virtualTarget"
        override fun execute(context: BeeExecuteContext, execute: Map<String, String>) {}
    }
}
