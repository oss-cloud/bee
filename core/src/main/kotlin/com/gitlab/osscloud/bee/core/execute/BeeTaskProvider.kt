package com.gitlab.osscloud.bee.core.execute

import com.github.dexecutor.core.task.ExecutionResult
import com.github.dexecutor.core.task.ExecutionResults
import com.github.dexecutor.core.task.Task
import com.github.dexecutor.core.task.TaskProvider
import com.gitlab.osscloud.bee.core.ExecuteTarget
import com.gitlab.osscloud.bee.spi.plugin.BeeExecuteContext
import org.slf4j.LoggerFactory
import java.io.IOException
import java.io.Serializable
import java.nio.file.FileSystems
import java.nio.file.FileVisitResult
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.SimpleFileVisitor
import java.nio.file.attribute.BasicFileAttributes
import kotlin.io.path.relativeTo

internal class BeeTaskProvider : TaskProvider<String, TaskOutput?> {
    private val taskMap: MutableMap<String, ExecutionTask> = hashMapOf()

    companion object {
        @JvmStatic private val log = LoggerFactory.getLogger(TaskProvider::class.java)
    }

    fun registerTask(target: ExecuteTarget, task: ExecutionTask) {
        registerTask(target.toString(), task)
    }

    fun registerTask(id: String, task: ExecutionTask) {
        log.info("register task {}", id)
        taskMap[id] = task
    }

    override fun provideTask(id: String): Task<String, TaskOutput?> {
        log.info("request task {}", id)
        if (taskMap.containsKey(id)) {
            return BeeTask(id, taskMap[id]!!)
        }
        throw RuntimeException("unknown target $id")
    }

    private class BeeTask(
        target: String,
        private val task: ExecutionTask,
    ) : Task<String, TaskOutput?>(), Serializable {
        private val log = LoggerFactory.getLogger("target:$target")

        override fun execute(): TaskOutput {
            val context = BeeExecuteContextImpl(task.cwd, task.path, parentResults)
            log.info("start executing")
            task.method.execute(context, task.args)
            log.info("execution complete")
            return TaskOutput(output = task.output.flatMap { globFiles(task.cwd, it) }.distinct())
        }

        private fun globFiles(root: Path, pattern: String): Set<Path> {
            val matcher = FileSystems.getDefault().getPathMatcher("glob:$pattern")
            val result = mutableSetOf<Path>()
            Files.walkFileTree(
                root,
                object : SimpleFileVisitor<Path>() {
                    override fun visitFile(file: Path, attrs: BasicFileAttributes?): FileVisitResult {
                        if (matcher.matches(file.relativeTo(root))) {
                            result.add(file)
                        }
                        return FileVisitResult.CONTINUE
                    }

                    override fun visitFileFailed(file: Path?, exc: IOException?) = FileVisitResult.CONTINUE
                }
            )
            return result
        }

        private class BeeExecuteContextImpl(
            override val cwd: Path,
            private val currentPath: String,
            results: ExecutionResults<String, TaskOutput?>,
        ) : BeeExecuteContext {
            private val outputs: Map<String, ExecutionResult<String, TaskOutput?>> = results.all.associateBy { it.id }

            override fun getEnv(name: String): String? {
                return System.getenv(name)
            }

            override fun resolveTargetOutput(target: String): List<Path>? {
                val key = ExecuteTarget.resolveWithContext(currentPath, target)
                    .toString()

                return outputs[key]!!.result?.output
            }
        }
    }
}
