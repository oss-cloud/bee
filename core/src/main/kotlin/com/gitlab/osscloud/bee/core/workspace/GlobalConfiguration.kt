package com.gitlab.osscloud.bee.core.workspace

import com.fasterxml.jackson.annotation.JsonProperty

internal data class GlobalConfiguration(
    val plugins: List<PluginConfiguration> = emptyList(),
)

internal data class PluginConfiguration(
    @JsonProperty("name") val methodName: String,
    val options: Map<String, String>
)
