package com.gitlab.osscloud.bee.core.execute

import java.nio.file.Path

internal data class TaskOutput(
    val output: List<Path>
)
