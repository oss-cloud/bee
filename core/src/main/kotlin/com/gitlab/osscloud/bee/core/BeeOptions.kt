package com.gitlab.osscloud.bee.core

import java.nio.file.Path

data class BeeOptions(
    val rootPath: Path,
    val concurrent: Int,
)
