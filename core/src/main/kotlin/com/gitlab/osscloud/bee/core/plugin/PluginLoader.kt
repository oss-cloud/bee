package com.gitlab.osscloud.bee.core.plugin

import com.gitlab.osscloud.bee.spi.plugin.Plugin
import com.gitlab.osscloud.bee.spi.plugin.PluginServiceProvider
import java.security.AccessController
import java.security.PrivilegedAction
import java.util.ServiceLoader

internal object PluginLoader {
    fun findPluginServiceProvider(): Sequence<Plugin> {
        return createLoader<PluginServiceProvider>(null)
            .asSequence()
            .map { it.plugin }
    }

    private inline fun <reified T> createLoader(classLoader: ClassLoader?): ServiceLoader<T> {
        return createLoader(T::class.java, classLoader)
    }

    private fun <T> createLoader(clazz: Class<T>, classLoader: ClassLoader?): ServiceLoader<T> {
        return if (System.getSecurityManager() == null) {
            if (classLoader == null) {
                ServiceLoader.load(clazz)
            } else {
                ServiceLoader.load(clazz, classLoader)
            }
        } else {
            AccessController.doPrivileged(
                PrivilegedAction {
                    if (classLoader == null) {
                        ServiceLoader.load(clazz)
                    } else {
                        ServiceLoader.load(clazz, classLoader)
                    }
                }
            )
        }
    }
}
