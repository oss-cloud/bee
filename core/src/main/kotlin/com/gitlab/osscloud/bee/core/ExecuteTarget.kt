package com.gitlab.osscloud.bee.core

data class ExecuteTarget(
    val path: String,
    val label: String,
) {
    companion object {
        @JvmStatic fun fromFullName(fullName: String): ExecuteTarget {
            require(fullName.contains(':')) { "label is required" }

            val pieces = fullName.split(':', limit = 2)
            return ExecuteTarget(path = pieces[0], label = pieces[1])
        }

        @JvmStatic fun resolveWithContext(currentPath: String, target: String): ExecuteTarget {
            return if (!target.startsWith("//")) {
                ExecuteTarget(path = currentPath, label = target.substring(1))
            } else {
                fromFullName(target)
            }
        }
    }

    override fun toString(): String {
        return "$path:$label"
    }
}
