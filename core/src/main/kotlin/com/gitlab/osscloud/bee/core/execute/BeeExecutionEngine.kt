package com.gitlab.osscloud.bee.core.execute

import com.github.dexecutor.core.DexecutorState
import com.github.dexecutor.core.ExecutionEngine
import com.github.dexecutor.core.ExecutionListener
import com.github.dexecutor.core.IdentifiableCallable
import com.github.dexecutor.core.concurrent.ExecutorCompletionService
import com.github.dexecutor.core.concurrent.IdentifiableRunnableFuture
import com.github.dexecutor.core.task.ExecutionResult
import com.github.dexecutor.core.task.Task
import com.github.dexecutor.core.task.TaskExecutionException
import org.slf4j.LoggerFactory
import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService
import java.util.concurrent.ScheduledExecutorService

internal class BeeExecutionEngine @JvmOverloads constructor(
    private val state: DexecutorState<String, TaskOutput?>,
    executorService: ExecutorService,
    private var listener: ExecutionListener<String, TaskOutput?>? = null,
) : ExecutionEngine<String, TaskOutput?> {
    companion object {
        @JvmStatic private val logger = LoggerFactory.getLogger(BeeExecutionEngine::class.java)
    }

    private var timeoutExecutor: ScheduledExecutorService? = null

    private val completionService = ExecutorCompletionService<String, ExecutionResult<String, TaskOutput>>(executorService)

    override fun isDistributed(): Boolean = false

    override fun isAnyTaskInError(): Boolean {
        return state.erroredCount() > 0
    }

    override fun processResult(): ExecutionResult<String, TaskOutput?> {
        val future = completionService.take() as IdentifiableRunnableFuture<String, ExecutionResult<String, TaskOutput>>
        val id = future.identifier
        return try {
            if (future.isCancelled) {
                val result = ExecutionResult.cancelled<String, TaskOutput>(id, "Task cancelled")
                state.removeErrored(result)
                result
            } else {
                future.get()
            }
        } catch (e: Exception) {
            throw TaskExecutionException("$id Task execution", e)
        }
    }

    override fun submit(task: Task<String, TaskOutput?>) {
        logger.debug("Received Task {}", task.id)
        completionService.submit(
            object : IdentifiableCallable<String, ExecutionResult<String, TaskOutput?>> {
                override fun getIdentifier(): String = task.id

                override fun call(): ExecutionResult<String, TaskOutput?> {
                    task.markStart()
                    var output: TaskOutput? = null
                    return try {
                        output = task.execute()
                        val result = ExecutionResult.success(task.id, output)
                        state.removeErrored(result)
                        task.markEnd()
                        listener?.onSuccess(task)
                        result
                    } catch (e: Exception) {
                        val result = ExecutionResult.errored(task.id, output, e.message)
                        state.addErrored(result)
                        task.markEnd()
                        listener?.onError(task, e)
                        logger.error("Error Execution Task # {}", task.id, e)
                        result
                    }
                }
            } as Callable<ExecutionResult<String, TaskOutput>>
        )
    }

    override fun setExecutionListener(listener: ExecutionListener<String, TaskOutput?>?) {
        this.listener = listener
    }

    override fun setTimeoutScheduler(timeoutExecutor: ScheduledExecutorService?) {
        this.timeoutExecutor = timeoutExecutor
    }
}
