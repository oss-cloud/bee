package com.gitlab.osscloud.bee.core.plan

import com.github.dexecutor.core.Dexecutor
import com.gitlab.osscloud.bee.core.execute.TaskOutput

internal data class Plan(
    val executor: Dexecutor<String, TaskOutput?>,
)
