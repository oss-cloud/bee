package com.gitlab.osscloud.bee.core.workspace

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.gitlab.osscloud.bee.core.Bee
import com.gitlab.osscloud.bee.core.BeeOptions
import com.gitlab.osscloud.bee.core.plugin.PluginLoader
import com.gitlab.osscloud.bee.core.plugin.PluginMethodProvider
import org.slf4j.LoggerFactory
import java.nio.file.Files
import java.nio.file.Path

internal object WorkspaceFactory {
    private val log = LoggerFactory.getLogger(this::class.java)
    private val objectMapper = ObjectMapper(YAMLFactory()).findAndRegisterModules().registerKotlinModule()

    fun build(options: BeeOptions): Workspace {
        val configPath = options.rootPath.resolve(Bee.ROOT_FILENAME)
        log.info("parse workspace configuration from {}", configPath)
        val wsConfig = objectMapper.readValue<GlobalConfiguration>(configPath.toFile())
        val pluginOptions = wsConfig.plugins.associate { it.methodName to it.options }
        return Workspace(
            rootPath = options.rootPath,
            concurrent = options.concurrent,
            pluginMethods = loadBuiltinPlugins(pluginOptions),
        )
    }

    private fun mkdir(path: Path) {
        try {
            Files.createDirectories(path)
        } catch (_: FileAlreadyExistsException) {}
    }

    private fun loadBuiltinPlugins(pluginOptions: Map<String, Map<String, String>>): Map<String, PluginMethodProvider> {
        return PluginLoader.findPluginServiceProvider()
            .flatMap { plugin ->
                plugin.availableMethods
                    .map { name ->
                        if (log.isDebugEnabled) {
                            log.debug("found plugin method: {}", name)
                        }
                        name to PluginMethodProvider(name, plugin, pluginOptions[name] ?: emptyMap())
                    }
            }
            .toMap()
    }
}
