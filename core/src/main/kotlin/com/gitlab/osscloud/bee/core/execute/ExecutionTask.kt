package com.gitlab.osscloud.bee.core.execute

import com.gitlab.osscloud.bee.spi.plugin.PluginMethod
import java.nio.file.Path

internal data class ExecutionTask(
    val path: String,
    val method: PluginMethod,
    val cwd: Path,
    val args: Map<String, String>,
    val output: Set<String>,
)
