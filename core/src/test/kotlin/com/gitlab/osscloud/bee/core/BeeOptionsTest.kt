package com.gitlab.osscloud.bee.core

import java.nio.file.Path
import kotlin.test.Test
import kotlin.test.assertEquals

class BeeOptionsTest {
    @Test
    fun `fields matching`() {
        val options = BeeOptions(
            rootPath = Path.of("/tmp"),
            concurrent = 4,
        )
        assertEquals("/tmp", options.rootPath.toString())
        assertEquals(4, options.concurrent)
    }
}
