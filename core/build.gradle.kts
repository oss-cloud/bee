plugins {
    jacoco

    kotlin("jvm")
}

tasks.getByName<Jar>("jar") {
    archiveBaseName.set("${project.group}.bee-core")
}

dependencies {
    implementation(project(":plugin-api"))

    implementation(kotlin("reflect"))
    testImplementation(kotlin("test"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:1.6.0")

    implementation(platform("com.fasterxml.jackson:jackson-bom:2.13.1"))
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml")
    implementation("com.fasterxml.jackson.core:jackson-databind")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    implementation("com.github.dexecutor:dexecutor-core:2.1.4")

    implementation(project(":plugins:shell"))
    implementation(project(":plugins:package"))
}
