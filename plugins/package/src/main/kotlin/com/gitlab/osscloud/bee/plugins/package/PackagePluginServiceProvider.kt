package com.gitlab.osscloud.bee.plugins.`package`

import com.gitlab.osscloud.bee.spi.plugin.Plugin
import com.gitlab.osscloud.bee.spi.plugin.PluginMethod
import com.gitlab.osscloud.bee.spi.plugin.PluginServiceProvider
import com.gitlab.osscloud.bee.spi.plugin.UnknownMethodException

class PackagePluginServiceProvider : PluginServiceProvider {
    override val plugin: Plugin = PackagePlugin

    private object PackagePlugin : Plugin {
        override val availableMethods: List<String> = listOf("zip")

        override fun createMethod(name: String, options: Map<String, String>): PluginMethod {
            return when (name) {
                "zip" -> ZipPluginMethod(options)
                else -> throw UnknownMethodException("unknown method $name")
            }
        }
    }
}
