package com.gitlab.osscloud.bee.plugins.`package`

import com.gitlab.osscloud.bee.spi.plugin.BeeExecuteContext
import com.gitlab.osscloud.bee.spi.plugin.PluginMethod
import org.slf4j.LoggerFactory
import java.io.FileInputStream
import java.nio.file.Files
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

internal class ZipPluginMethod(private val options: Map<String, String>) : PluginMethod {
    companion object {
        @JvmStatic private val logger = LoggerFactory.getLogger(ZipPluginMethod::class.java)
    }

    override val name: String = "zip"

    override fun execute(context: BeeExecuteContext, execute: Map<String, String>) {
        Files.newOutputStream(context.cwd.resolve(execute["output"]!!)).use { output ->
            ZipOutputStream(output).use { out ->
                if (options.containsKey("level")) {
                    out.setLevel(Integer.parseInt(options["level"]!!))
                }
                val source = execute["source"]!!
                (if (source.startsWith("@//")) {
                    context.resolveTargetOutput(source.substring(1))!!
                } else {
                    listOf(context.cwd.resolve(source))
                }).forEach {
                    val file = it.toFile()
                    logger.info("adding file {} to zip", file)
                    out.putNextEntry(ZipEntry(file.name))
                    FileInputStream(file)
                        .use { fs -> fs.transferTo(out) }
                    out.closeEntry()
                }
            }
        }
    }
}
