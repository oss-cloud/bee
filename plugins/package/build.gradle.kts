plugins {
    kotlin("jvm")
}

tasks.getByName<Jar>("jar") {
    archiveBaseName.set("${project.group}.bee-plugin-package")
}

dependencies {
    implementation(project(":plugin-api"))
}
