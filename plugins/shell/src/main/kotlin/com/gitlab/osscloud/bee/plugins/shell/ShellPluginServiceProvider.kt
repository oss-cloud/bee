package com.gitlab.osscloud.bee.plugins.shell

import com.gitlab.osscloud.bee.spi.plugin.BeeExecuteContext
import com.gitlab.osscloud.bee.spi.plugin.Plugin
import com.gitlab.osscloud.bee.spi.plugin.PluginException
import com.gitlab.osscloud.bee.spi.plugin.PluginMethod
import com.gitlab.osscloud.bee.spi.plugin.PluginServiceProvider
import com.gitlab.osscloud.bee.spi.plugin.UnknownMethodException
import java.lang.ProcessBuilder.Redirect

class ShellPluginServiceProvider : PluginServiceProvider {
    override val plugin: Plugin = ShellPlugin
    private object ShellPlugin : Plugin {
        override val availableMethods: List<String> = listOf("shell")

        override fun createMethod(name: String, options: Map<String, String>): PluginMethod {
            return when (name) {
                "shell" -> ShellPluginMethod(options)
                else -> throw UnknownMethodException(name)
            }
        }

        private class ShellPluginMethod(
            private val options: Map<String, String>
        ) : PluginMethod {
            override val name: String = "shell"

            override fun execute(context: BeeExecuteContext, execute: Map<String, String>) {
                val exitCode = ProcessBuilder(execute["cmd"]!!.split(Regex("\\s"))).apply {
                    directory(context.cwd.toFile())
                    redirectOutput(Redirect.INHERIT)
                    redirectError(Redirect.INHERIT)
                }
                    .start()
                    .waitFor()

                if (exitCode != 0) {
                    throw PluginException("failed execution")
                }
            }
        }
    }
}
