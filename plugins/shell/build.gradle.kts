plugins {
    kotlin("jvm")
}

tasks.getByName<Jar>("jar") {
    archiveBaseName.set("${project.group}.bee-plugin-shell")
}

dependencies {
    implementation(project(":plugin-api"))
}
