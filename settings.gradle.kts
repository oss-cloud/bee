rootProject.name = "bee"

include("cli")
include("core")
include("plugin-api")
include("plugins:shell")
include("plugins:package")
