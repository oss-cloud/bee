plugins {
    kotlin("jvm")
}

tasks.getByName<Jar>("jar") {
    archiveBaseName.set("${project.group}.bee-plugin-api")
}

dependencies {
    api("org.slf4j:slf4j-api:1.7.36")
}
