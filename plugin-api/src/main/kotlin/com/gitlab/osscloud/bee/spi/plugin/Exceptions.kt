package com.gitlab.osscloud.bee.spi.plugin

open class PluginException : RuntimeException {
    constructor() : super()
    constructor(message: String) : super(message)
    constructor(cause: Throwable) : super(cause)
    constructor(message: String, cause: Throwable) : super(message, cause)
}

class UnknownMethodException(name: String) : PluginException("unknown plugin method \"$name\"")
