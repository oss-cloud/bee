package com.gitlab.osscloud.bee.spi.plugin

interface PluginServiceProvider {
    val plugin: Plugin
}

interface Plugin {
    val availableMethods: List<String>
    fun createMethod(name: String, options: Map<String, String>): PluginMethod
}

interface PluginMethod {
    val name: String
    fun execute(context: BeeExecuteContext, execute: Map<String, String>)
}
