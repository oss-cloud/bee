package com.gitlab.osscloud.bee.spi.plugin

import java.nio.file.Path

interface BeeExecuteContext {
    val cwd: Path
    fun getEnv(name: String): String?
    fun resolveTargetOutput(target: String): List<Path>?
}
