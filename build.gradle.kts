plugins {
    val kotlinVersion = "1.6.10"
    kotlin("jvm") version kotlinVersion

    id("com.diffplug.spotless") version "6.3.0"
}

allprojects {
    group = "com.gitlab.osscloud.bee"
    version = "1.0-SNAPSHOT"

    repositories {
        mavenCentral()
    }

    apply(plugin = "com.diffplug.spotless")
    spotless {
        kotlinGradle {
            ktlint("0.44.0")
        }
    }
}

subprojects {
    pluginManager.withPlugin("org.jetbrains.kotlin.jvm") {
        spotless {
            kotlin {
                ktlint("0.44.0")
            }
        }
    }

    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = "11"
        }
    }

    tasks.withType<Test> {
        useJUnitPlatform()
    }
}
