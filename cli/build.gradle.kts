plugins {
    application
    kotlin("jvm")
}

tasks.getByName<Jar>("jar") {
    archiveBaseName.set("${project.group}.bee-cli")
}

application {
    applicationName = "bee"
    mainClass.set("com.gitlab.osscloud.bee.cli.MainKt")
}

dependencies {
    implementation(project(":core"))

    testImplementation(kotlin("test"))

    implementation("org.slf4j:slf4j-api:1.7.36")
    implementation("ch.qos.logback:logback-classic:1.2.10")

    implementation("org.jetbrains.kotlinx:kotlinx-cli:0.3.4")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:1.6.0")
}
