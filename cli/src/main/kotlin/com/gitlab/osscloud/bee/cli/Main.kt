package com.gitlab.osscloud.bee.cli

import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType
import kotlinx.cli.vararg
import org.slf4j.LoggerFactory
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    val log = LoggerFactory.getLogger("main")

    val parser = ArgParser("bee")

    val root by parser.option(ArgType.String, fullName = "root", description = "Root path of project")
    val concurrent by parser.option(ArgType.Int, fullName = "concurrent", description = "Maximum concurrent")
    val targets by parser.argument(ArgType.String, fullName = "targets").vararg()

    parser.parse(args)

    val bee = BeeFactory.build(root = root, concurrent = concurrent)
    try {
        bee.buildForTargets(targets)
        exitProcess(0)
    } catch (t: Throwable) {
        log.error("failed to build: {}", t)
        exitProcess(1)
    }
}
