package com.gitlab.osscloud.bee.cli

import com.gitlab.osscloud.bee.core.Bee
import com.gitlab.osscloud.bee.core.BeeOptions
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

internal object BeeFactory {
    fun build(
        root: String? = null,
        concurrent: Int? = null,
    ): Bee {
        val options = BeeOptions(
            rootPath = if (root != null) {
                Path.of(root).apply { require(isProjectRoot) { "cannot find root path" } }
            } else {
                findRoot()
            },
            concurrent = concurrent ?: (Runtime.getRuntime().availableProcessors() + 1),
        )
        return Bee(options)
    }

    private fun findRoot(): Path {
        var current = Paths.get("").toAbsolutePath()
        val root = current.root
        while (!current.isProjectRoot) {
            if (current == root) {
                throw RuntimeException("failed to find root path")
            }
            current = current.parent
        }
        return current
    }

    private val Path.isProjectRoot: Boolean
        get() = Files.exists(resolve(Bee.ROOT_FILENAME))
}
